﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Midterm
{
	public partial class Search
	{
        [JsonProperty("bestMatches")]
        public Search1[] bestMatches
        {
            get;
            set;
        }
       
		
	}
    public partial class Search1
    {
        [JsonProperty("1. symbol")]
        public string symbol
        {
            get;
            set;
        }
        [JsonProperty("2. name")]
        public string name
        {
            get;
            set;
        }
        public string type
        {
            get;
            set;
        }
        public string region
        {
            get;
            set;
        }
        public string marketOpen
        {
            get;
            set;
        }
        public string marketClose
        {
            get;
            set;
        }
        public string timezone
        {
            get;
            set;
        }
        public string currency
        {
            get;
            set;
        }
        public string matchScore
        {
            get;
            set;
        }
    }
}