﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Midterm
{

    public partial class App : Application
    {
        public static Dictionary<string, TimeSeriesDaily> Stockss { get; set; } = new Dictionary<string, TimeSeriesDaily>();
        public App()
        {
            InitializeComponent();
          

        MainPage = new TabbedPage1();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
