﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;


using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Midterm
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class HighsLows : ContentPage
	{
        private object sender;
        private EventArgs e;
        public static  StockModel stockdata;
       // public static ObservableCollection<TimeSeriesDaily> information;
  
        public HighsLows ()
		{

			InitializeComponent ();
            //Daily();
          //  SearchStock_SearchButtonPressed_1(sender, e);


        }

        //async void MenuItem_Clicked(object sender, EventArgs e)
        async void Daily(string symbols)
        {
          
           
            
            
        }

        async void SearchStock_TextChanged(object sender, TextChangedEventArgs e) //Wanted to implement this but would lag the app
        {
              
            /*
            var Client = new HttpClient();
            var StockApiAddress = "https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=" + SearchStock.Text + "&apikey=4J6ZSG2R5KR2SW9S";
            var uri = new Uri(StockApiAddress);

            var stockdata = new Search();
            var response = await Client.GetAsync(uri);
            if(response.IsSuccessStatusCode)
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
                stockdata = JsonConvert.DeserializeObject<Search>(jsonContent);
            }
          
            SearchView.ItemsSource = stockdata.bestMatches;
            */
        }

      

        private void SearchStock_SearchButtonPressed_1(object sender, EventArgs e)
        {
            //var userInput = SearchStock.Text;
            //Daily();
        }

        async void Button_Clicked(object sender, EventArgs e)
        {



            //Daily(SearchStock.Text);
            var Client = new HttpClient();

            var StockApiAddress = "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + SearchStock.Text + "&apikey=4J6ZSG2R5KR2SW9S";
            var uri = new Uri(StockApiAddress);
           // var stockdata = new StockModel();
            stockdata = new StockModel();
            var response = await Client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
                stockdata = JsonConvert.DeserializeObject<StockModel>(jsonContent);
            }
          
          
            StockList.ItemsSource = stockdata.TimeSeriesDaily;
          // App.Stockss = stockdata.TimeSeriesDaily;
            if (stockdata.MetaData == null)
            {
                await DisplayAlert("Invalid Symbol", "symbol not found", "try again");
            }
            //information = new ObservableCollection<TimeSeriesDaily>(stockdata.TimeSeriesDaily.Values);
            //StockList.ItemsSource = information;

            if (stockdata.TimeSeriesDaily != null)
            {
                SymbolText.Text = SearchStock.Text;
                HighestValue.Text = stockdata.TimeSeriesDaily.Values.Max(a => a.High);
                LowestValue.Text = stockdata.TimeSeriesDaily.Values.Min(a => a.Low);
            }
            if (stockdata.MetaData != null)
            {
                foreach (var item in stockdata.TimeSeriesDaily)
                {
                    item.Value.date = item.Key;
                }
            }

        }

        private void StockList_Refreshing(object sender, EventArgs e)
        {
            StockList.IsRefreshing = false;
        }

        private void Button_Clicked_1(object sender, EventArgs e)
        {
         
        }
    }
}