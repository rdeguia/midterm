﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microcharts;
using SkiaSharp;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Entry = Microcharts.Entry;
using System.Net.Http;
using System.Collections.ObjectModel;
using Newtonsoft.Json;

namespace Midterm
{

    public partial class Graphs : ContentPage
    {
        int count;
        public Graphs()
        {
            InitializeComponent();
            
        }


        private void CreateChart()
        {
            var en = new List<Entry>();
            if (HighsLows.stockdata != null)
            {
                foreach (var stockss in HighsLows.stockdata.TimeSeriesDaily.Values)
                {
                    count = count + 1;
                    if (count % 6 == 0)
                    {


                        var createnew = new Entry(float.Parse(stockss.High))
                        {
                            Color = SKColor.Parse("FF0000"),
                            ValueLabel = stockss.High,
                        };
                        en.Add(createnew);
                        en.Reverse();
                    }
                }
            }
            var linechart = new LineChart()
            {
                Entries = en,
            };
            StockChart.Chart = linechart;
        }


       void ContentPage_Appearing_1(object sender, EventArgs e)
        {
            var x = HighsLows.stockdata;
            CreateChart();
        }
    }
}
